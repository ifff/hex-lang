## hex
A language that compiles to LLVM. My plans for it are rapidly changing, so trust nothing I say about it -- but that being said, you might want to look at the plans.org file for more information.

## will this language grow big and popular?
no.

## helpful resources I used
- [generic unification](https://ro-che.info/articles/2017-06-17-generic-unification)
  - by Roman Cheplyaka
- [kaleidoscope tutorial](https://www.stephendiehl.com/llvm/#instructions)
  - by Stephen Diehl
  - much of the LLVM codegen is based off of this
