{-# LANGUAGE TypeApplications #-}

module Hex.Parser (parseHex) where

import Numeric (readHex)

import Control.Monad (replicateM)

import Data.Functor.Fixedpoint (Fix(..))
import Data.Functor.Identity (Identity)
import Data.Word (Word8, Word32)

import Text.Parsec
import Text.Parsec.Expr
import Text.Parsec.Char
import Text.Parsec.String (Parser)
import qualified Text.Parsec.Token as T

import Hex.Term


parseHex :: String -> Either ParseError [Term]
parseHex = parse (many term) "stdin"


-- terms that are allowed to occur at the top level in code
term :: Parser Term
term = defn <?> "top-level term"

-- terms that can be used as a value
val :: Parser Term
val = parens expr <|> num <|> void <|> var <?> "value"

-- expressions
-- (see https://hackage.haskell.org/package/parsec-3.0.0/docs/Text-Parsec-Expr.html)
expr = buildExpressionParser table val <?> "expression"

-- TODO: pad operator with spaces
table = [ [binary "+" (binOp "+") AssocLeft] ]
  where binOp s a b = Fix (Apply s [a, b])

binary name fn assoc = Infix  (reservedOp name >> return fn) assoc
prefix name fn       = Prefix (reservedOp name >> return fn)


-- terms that count as "statements"
stmt :: Parser Term
stmt = do
  s <- try ret <|> def
  semi
  return s

stmts :: Parser [Term]
stmts = many1 stmt


void :: Parser Term
void = reserved "void" >> returnF Void

var :: Parser Term
var = do
  s <- ident
  returnF (Var s)

num :: Parser Term
num = double

double :: Parser Term
double = do
  min <- emptyOr (char '-')
  ds1 <- many1 digit
  dot <- emptyOr (char '.')
  ds2 <- many digit
  let s = min <> ds1 <> dot <> ds2
      d = read @Double s
  returnF (Dbl d)

def :: Parser Term
def = do
  i <- ident
  symbol ":="
  v <- expr
  returnF (Def i v)

defn :: Parser Term
defn = do
  reserved "func"
  i <- ident
  -- TODO: allow pointer argument syntax, and maybe add semantic info for it
  args <- parens (commaSep ident)
  body <- braces stmts
  returnF (FnDef i args body)

ret :: Parser Term
ret = do
  reserved "return"
  v <- expr
  returnF (Ret v)
    


-- other parsers
emptyOr :: Parser Char -> Parser String
emptyOr p = fmap (:[]) p <|> return ""

-- | Convert hex chars to a numeric type.
hexify :: (Eq a, Num a) => [Char] -> a
hexify xs = fst (head a)
  where a = readHex xs

-- | Parse a newline as a string rather than a char.
snewline :: Parser String
snewline = newline >>= \a -> return [a]


hexDef = T.LanguageDef
  { T.commentLine = ";;"
  , T.nestedComments = False
  , T.identStart = letter
  , T.identLetter = alphaNum
  , T.caseSensitive = True
  , T.commentStart = ""
  , T.commentEnd = ""
  , T.opStart = oneOf "+"
  , T.opLetter = oneOf "+"
  , T.reservedOpNames = ["+"]
  , T.reservedNames = ["void", "func", "var", "return"] }

lexer = T.makeTokenParser hexDef

ident = T.identifier lexer
reserved = T.reserved lexer
reservedOp = T.reservedOp lexer
parens = T.parens lexer
angles = T.angles lexer
braces = T.braces lexer
whitespace = T.whiteSpace lexer
str = T.stringLiteral lexer
symbol = T.symbol lexer
decimal = T.decimal lexer
commaSep = T.commaSep lexer
semi = T.semi lexer

-- for returning a @Fix@ed thing
returnF = return . Fix
