let pkgs = import <nixpkgs> {};
in {
  hex-lang = pkgs.haskellPackages.callPackage ./hex-lang.nix {};
}
