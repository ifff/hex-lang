module Hex.Codegen.Util where

import Control.Monad.State

import Data.List (sortBy)
import Data.Function (on)
import qualified Data.Map as Map

import LLVM.AST
import LLVM.AST.Global as G
import qualified LLVM.AST as AST
import qualified LLVM.AST.Attribute as A
import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.Float as F

import Hex.Codegen.Types
import Hex.Util


---------------------------------------------------------------
-- IDENTIFIERS
---------------------------------------------------------------
-- | Get a fresh number for use in an identifier.
fresh :: Codegen Word
fresh = do
  i <- gets count
  modify $ \s -> s { count = i + 1 }
  return (i + 1)

-- | Create a unique name and save it in the list of names.
-- If the provided name already exists in the list of names, it will get a number
-- added to the end of it, making it unique.
uniqueName :: String -> Names -> (String, Names)
uniqueName nm nms =
  case Map.lookup nm nms of
    Nothing -> (nm, Map.insert nm 1 nms)
    Just ix -> (nm ++ show ix, Map.insert nm (ix+1) nms)


---------------------------------------------------------------
-- GLOBAL DEFINITIONS
---------------------------------------------------------------
-- | Record a new global function definition.
addDef :: Definition -> LLVM ()
addDef d = do
  defs <- gets moduleDefinitions
  modify $ \s -> s { moduleDefinitions = defs ++ [d] }

-- | Record a new global function definition.
define :: Type -> String -> [(Type, Name)] -> [BasicBlock] -> LLVM ()
define retty lbl argtys body = addDef $
  GlobalDefinition $ functionDefaults
  { name        = Name (toShort lbl)
  , parameters  = ([Parameter ty nm [] | (ty, nm) <- argtys], False)
  , returnType  = retty
  , basicBlocks = []
  }

-- | Record a new global variable definition.
define' :: Type -> String -> C.Constant -> LLVM ()
define' ty nm val = addDef $
  GlobalDefinition $ globalVariableDefaults
  { name       = Name (toShort nm)
  , G.type'    = double -- as if!
  , initializer = Just val
  }


---------------------------------------------------------------
-- LOCAL VARIABLES
---------------------------------------------------------------
-- | Record a new local variable.
assign :: String -> Operand -> Codegen ()
assign var x = do
  lcls <- gets symtab
  modify $ \s -> s { symtab = (var, x) : lcls }

-- | Look up a local variable by name.
getvar :: String -> Codegen Operand
getvar var = do
  lcls <- gets symtab
  case lookup var lcls of
    Just x  -> return x
    Nothing -> error ("Local variable not in scope: " ++ show var)


---------------------------------------------------------------
-- BLOCKS
---------------------------------------------------------------
-- | Create a new empty block with the given index.
emptyBlock :: Int -> BlockState
emptyBlock i = BlockState i [] Nothing

-- | Add a new empty block with the given name.
addBlock :: String -> Codegen Name
addBlock nm = do
  bs  <- gets blocks
  ix  <- gets nBlocks
  nms <- gets names
  -- create a new block with a unique name
  let new = emptyBlock ix
      (qname, supply) = uniqueName nm nms
      qname' = toShort qname
  -- increment the number of blocks, add our new block to the list,
  -- and update the name mapping to include the new name
  modify $ \s -> s { blocks = Map.insert (Name qname') new bs
                   , nBlocks = ix + 1
                   , names = supply }
  return (Name qname')

-- | Set the current block's name.
setBlock :: Name -> Codegen Name
setBlock nm = do
  modify $ \s -> s { currentBlock = nm }
  return nm

-- | Get the current block's name.
getBlock :: Codegen Name
getBlock = gets currentBlock

-- | Update the current block's state by setting it to a new value.
modifyBlock :: BlockState -> Codegen ()
modifyBlock new = do
  b <- gets currentBlock
  modify $ \s -> s { blocks = Map.insert b new (blocks s) }

-- | Get the current block's state.
current :: Codegen BlockState
current = do
  c  <- gets currentBlock
  bs <- gets blocks
  case Map.lookup c bs of
    Just x  -> return x
    Nothing -> error ("No such block: " ++ show c)

-- | Sort a list of named blocks by index.
sortBlocks :: [(Name, BlockState)] -> [(Name, BlockState)]
sortBlocks = sortBy (compare `on` (idx . snd))

-- | Turn all the blocks saved in the codegen state into @BasicBlock@s.
createBlocks :: CodegenState -> [BasicBlock]
createBlocks m = map mkBlock sortedBlocks
  where sortedBlocks = sortBlocks bs
        bs = Map.toList (blocks m)

-- | Convert a named block from our representation to LLVM's @BasicBlock@ representation.
mkBlock :: (Name, BlockState) -> BasicBlock
mkBlock (l, (BlockState _ s t)) = BasicBlock l s' t'
  where s' = reverse s
        t' = mkTerm t
        mkTerm (Just x) = x
        mkTerm Nothing  = error ("Block has no terminator: " ++ show l)



---------------------------------------------------------------
-- TRANSDUCERS
---------------------------------------------------------------
-- TODO: this will eventually be modified once we're using more
-- types than just doubles
-- | Turn a list of argument names into part of a function signature.
toSig :: [String] -> [(AST.Type, AST.Name)]
toSig = map (\x -> (double, AST.Name (toShort x)))

-- | Turn a list of operands into a list of arguments.
toArgs :: [Operand] -> [(Operand, [A.ParameterAttribute])]
toArgs = map (\x -> (x, []))


---------------------------------------------------------------
-- CONSTANT TYPES
---------------------------------------------------------------
dbl :: Double -> C.Constant
dbl n = C.Float (F.Double n)


---------------------------------------------------------------
-- OPERAND TYPES
---------------------------------------------------------------
con :: C.Constant -> Operand
con = ConstantOperand

local :: Name -> Operand
local = LocalReference double -- TODO: types

externf :: Name -> Operand
externf = ConstantOperand . C.GlobalReference double -- TODO: types
