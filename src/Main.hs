module Main (main) where

import qualified LLVM.AST as AST

import System.Exit (exitSuccess)
import Control.Monad (when)
import System.Environment (getArgs)
import System.IO (readFile)

import Hex.Analysis.Typecheck
import Hex.Codegen
import Hex.Codegen.Types (emptyModule)
import Hex.Parser
import Hex.Pretty


newModule :: AST.Module
newModule = emptyModule "hex~~"


main :: IO ()
main = do
  -- get the arguments
  args <- getArgs
  when (length args /= 1) $ do
    putStrLn "usage: hx [file]"
    exitSuccess

  -- read in the source file
  let [ipath] = args
  src <- readFile ipath
  
  case parseHex src of
    Left  err -> putStrLn ("parse error: " <> show err)
    Right ast -> do
      -- print parsed ast
      putStrLn ("compiling\n " <> pretty' ast <> "\n...")

      -- run the ast thru some analysis steps
      let result = execTypecheck (mapM tc ast)
      putStrLn "done typechecking."

      -- jit the code
      codegen newModule result
      return ()
