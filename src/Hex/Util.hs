module Hex.Util where

import qualified Data.ByteString.UTF8 as BU (fromString)
import qualified Data.ByteString.Short as BS (ShortByteString, toShort)


-- ughhh why do haskell strings have to be like this
-- | Convert a @String@ to a @ShortByteString@.
toShort :: String -> BS.ShortByteString
toShort = BS.toShort . BU.fromString
