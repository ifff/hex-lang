{-# LANGUAGE OverloadedStrings  #-}

module Hex.JIT where

import qualified Data.ByteString.Char8 as B (putStrLn)
import Foreign.Ptr (FunPtr, castFunPtr)

import LLVM.Target
import LLVM.Context
import LLVM.Module as Mod
import qualified LLVM.AST as AST

import LLVM.PassManager

import qualified LLVM.ExecutionEngine as EE


foreign import ccall "dynamic" haskFun :: FunPtr (IO Double) -> IO Double

-- TODO: use libffi for determining arg types at runtime
run :: FunPtr a -> IO Double
run fn = haskFun (castFunPtr fn :: FunPtr (IO Double))

-- | Optimizes and runs an LLVM module.
runJIT :: AST.Module -> IO AST.Module
runJIT mod = do
  withContext $
    \ctx -> do
      initializeAllTargets
      jit ctx $
        \executionEngine ->
          withModuleFromAST ctx mod $
        \m -> withPassManager passes $
              \pm -> do
                -- optimization pass
                _ <- runPassManager pm m
                optmod <- moduleAST m
                s <- moduleLLVMAssembly m
                B.putStrLn s

                EE.withModuleInEngine executionEngine m $
                  \ee -> do
                    mainfn <- EE.getFunction ee (AST.Name "main")
                    case mainfn of
                      Just fn -> do
                        res <- run fn
                        putStrLn ("Evaluated to: " ++ show res)
                      Nothing -> return ()

                -- return the optimized module
                return optmod
                                                           
      
-- | The specification for the LLVM passes to run.
passes :: PassSetSpec
passes = defaultCuratedPassSetSpec { optLevel = Just 3 }
      
jit :: Context -> (EE.MCJIT -> IO a) -> IO a
jit c = EE.withMCJIT c optlvl model ptrelim fastins
  where optlvl  = Just 2  -- optimization lvl
        model   = Nothing -- code model (default)
        ptrelim = Nothing -- frame pointer elim
        fastins = Nothing -- fast instr selection
