{-# LANGUAGE DeriveFoldable       #-}
{-# LANGUAGE DeriveFunctor        #-}
{-# LANGUAGE DeriveTraversable    #-}
{-# LANGUAGE RecordWildCards      #-}
{-# LANGUAGE StandaloneDeriving   #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Hex.Term where

import Data.Functor.Fixedpoint (Fix(..))
import Data.List (intercalate)
import Data.Word (Word8)
import Numeric (showHex)

import Hex.Pretty


-- | The different possible values in the Hex language.
data TermF a = Dbl  { tdbl_val :: Double }
             | Def  { tdef_name :: String, tdef_val :: a }
             | Ret  { tret_val :: a }
             | Var  { tvar_name :: String }
             | FnDef { tfn_name :: String, tfn_args :: [String], tfn_body :: [a] }
             | Apply { tap_name :: String, tap_args :: [a] }
             | Void
  deriving (Eq, Functor, Ord, Show)
-- deriving (Eq, Foldable, Functor, Ord, Show, Traversable)

data Annotate f a = Annotate { ann :: a, layer :: (f (Annotate f a)) }

type Term    = Fix TermF
type ATerm a = Annotate TermF a

-- | Remove the annotations from an @ATerm@, converting it to a bare @Term@.
strip :: ATerm a -> Term
strip Annotate{..} = Fix (fmap strip layer)

-- | Annotate a term with data. The subterms of the term should already be annotated.
annotate :: a -> TermF (ATerm a) -> ATerm a
annotate a t = Annotate { ann = a, layer = t }


instance (Pretty a) => Pretty (TermF a) where
  pretty Dbl{..} = show tdbl_val
  pretty Def{..} = printf "%s := %s" tdef_name val
    where val = pretty tdef_val
  pretty FnDef{..} = printf "func %s(%s) {\n%s\n}" tfn_name args body
    where args = intercalate ", " tfn_args
          body = intercalate "\n" (map pretty tfn_body)
  pretty Ret{..} = "return " ++ pretty tret_val
  pretty    Void = "void"
  pretty Var{..} = tvar_name
  pretty Apply{..} =
    case (tap_name, tap_args) of
      ("+", [a, b]) -> printf "(%s + %s)" (pretty a) (pretty b)
      _ ->
        let args = intercalate ", " (map pretty tap_args)
        in printf "%s()" tap_name args

deriving instance Pretty Term

instance (Pretty a) => Pretty (Annotate TermF a) where
  pretty Annotate{..} = printf "%s : %s" slayer sann
    where slayer = pretty layer
          sann   = pretty ann
