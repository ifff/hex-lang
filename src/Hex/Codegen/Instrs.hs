module Hex.Codegen.Instrs where

import LLVM.AST
import LLVM.AST.Global
import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.CallingConvention as CC

import Hex.Codegen.Types
import Hex.Codegen.Util

---------------------------------------------------------------
-- INSTRUCTION TYPES
---------------------------------------------------------------
instr :: Instruction -> Codegen (Operand)
instr ins = do
  n <- fresh
  let ref = UnName n
  b <- current
  let i = stack b
  modifyBlock (b { stack = (ref := ins) : i })
  return (local ref)

terminator :: Named Terminator -> Codegen (Named Terminator)
terminator t = do
  b <- current
  modifyBlock (b { term = Just t })
  return t


---------------------------------------------------------------
-- FLOAT MATH
---------------------------------------------------------------
fadd :: Operand -> Operand -> Codegen Operand
fadd a b = instr $ FAdd noFastMathFlags a b []

fsub :: Operand -> Operand -> Codegen Operand
fsub a b = instr $ FSub noFastMathFlags a b []

fmul :: Operand -> Operand -> Codegen Operand
fmul a b = instr $ FMul noFastMathFlags a b []

fdiv :: Operand -> Operand -> Codegen Operand
fdiv a b = instr $ FDiv noFastMathFlags a b []


---------------------------------------------------------------
-- CONTROL FLOW
---------------------------------------------------------------
br :: Name -> Codegen (Named Terminator)
br val = terminator $ Do (Br val [])

cbr :: Operand -> Name -> Name -> Codegen (Named Terminator)
cbr cond tr fl = terminator $ Do (CondBr cond tr fl [])

ret :: Operand -> Codegen (Named Terminator)
ret val = terminator $ Do (Ret (Just val) [])

call :: Operand -> [Operand] -> Codegen Operand
call f args = instr $ Call Nothing CC.C [] (Right f) (toArgs args) [] []


---------------------------------------------------------------
-- MEMORY
---------------------------------------------------------------
alloca :: Type -> Codegen Operand
alloca ty = instr (Alloca ty Nothing 0 [])

store :: Operand -> Operand -> Codegen Operand
store ptr val = instr (Store False ptr val Nothing 0 [])

load :: Operand -> Codegen Operand
load ptr = instr (Load False ptr Nothing 0 [])
