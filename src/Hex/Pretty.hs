module Hex.Pretty (Pretty(..), pretty', printf) where

import Data.List (intercalate)
import Data.Word (Word8, Word32)
import Numeric (showHex)
import Text.Printf (printf)

-- | A super-simple typeclass for pretty-printing behavior.
class Pretty a where
  pretty :: a -> String


-- some instances
instance Pretty Word8 where
  pretty n = showHex n "0x"

instance Pretty Word32 where
  pretty n = showHex n "0x"

instance Pretty Int where
  pretty n = show n

instance (Pretty a) => Pretty [a] where
  pretty xs = intercalate " " (map pretty xs)

-- | Pretty-print items in a list, separated by newlines
pretty' :: (Pretty a) => [a] -> String
pretty' xs = intercalate "\n" (map pretty xs)
