module Main (main) where

import System.Exit
import Test.HUnit
import Hex.Parser
import Hex.Pretty
import Hex.Term

main :: IO ()
main = do
  counts <- runTestTT tests
  if (errors counts + failures counts) == 0
    then exitSuccess
    else exitFailure


tests = test [pprint ++ parse]


pprint = [ "pprint1" ~: "nested appl" ~:
           "((+ x 10.0) y)" ~=?
           pretty (Appl (Appl (Symbol "+") [Symbol "x", Num 10]) [Symbol "y"]),
           
           "pprint2" ~: "empty enchantment" ~:
           "enchant:{}" ~=? pretty (Enchant []),
           
           "pprint2" ~: "enchantment" ~:
           "enchant:{~(+ x y) #1 ~2.0 #2}" ~=?
           pretty (Enchant [RSplice (Appl (Symbol "+") [Symbol "x", Symbol "y"]),
                            Rune8 0x01, RSplice (Num 2.0), Rune8 0x02]) ]


parse = [ "parse1" ~: "nested appl" ~:
          Right [Appl (Appl (Symbol "+") [Symbol "x", Num 10]) [Symbol "y"]] ~=?
          parseHex "((+ x 10) y)",
          
          "parse2" ~: "base-16 number" ~:
          Right [Num 17] ~=? parseHex "#11",
          
          "parse3" ~: "symbol" ~:
          Right [Symbol "let~$-all-l0ve-lain"] ~=?
          parseHex "let~$-all-l0ve-lain",

          "parse4" ~: "empty enchantment" ~:
          "enchant:{}" ~=? pretty (Enchant []),
          
          "parse5" ~: "enchantment" ~:
          "enchant:{~(+ x y) #1 ~2.0 #2}" ~=?
          pretty (Enchant [RSplice (Appl (Symbol "+") [Symbol "x", Symbol "y"]),
                           Rune8 0x01, RSplice (Num 2.0), Rune8 0x02])]
  
