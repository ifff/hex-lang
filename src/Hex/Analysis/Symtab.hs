{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Hex.Analysis.Symtab
  ( SymbolT(..)
  , SymtabState(..)
  , newSymtab
  , runSymbolT
  , find
  , insert
  , pushScope
  , popScope ) where


import Control.Monad.State
import qualified Data.Map as M


-- | The monad transformer for a stateful symbol table.
newtype SymbolT m a b = Symtab { unSymbolT :: StateT (SymtabState a) m b }
  deriving (Functor, Applicative, Monad, MonadState (SymtabState a))

-- | The state of a symbol table.
data SymtabState a = MkSymtab { tables :: [M.Map String a] }
  deriving (Show)


-- | Creates an empty symbol table.
newSymtab :: SymtabState a
newSymtab = MkSymtab [M.empty]

-- | Runs a @SymbolT@ action and returns the state and result inside the next monad.
runSymbolT :: (Monad m) => SymbolT m a b -> m (b, SymtabState a)
runSymbolT s = runStateT (unSymbolT s) newSymtab


-- | Looks up a symbol by name.
find :: (Monad m) => String -> SymbolT m a (Maybe a)
find s = gets (M.lookup s . head . tables)

-- | Inserts a symbol's information by name.
insert :: (Monad m) => String -> a -> SymbolT m a ()
insert s a = modifyTbls $ \(t:ts) -> (M.insert s a t):ts


-- | Adds another scope level to the symbol table.
pushScope :: (Monad m) => SymbolT m a ()
pushScope = modifyTbls $ \(t:ts) -> (t:t:ts)

-- | Removes the current scope level from the symbol table.
popScope :: (Monad m) => SymbolT m a ()
popScope = modifyTbls tail


-- a utility function for modifying the internal symtab representation more easily.
modifyTbls :: (Monad m) => ([M.Map String a] -> [M.Map String a]) -> SymbolT m a ()
modifyTbls f = modify $ \(MkSymtab t) -> MkSymtab (f t)
