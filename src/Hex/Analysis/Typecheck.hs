{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE DeriveFoldable    #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards   #-}


module Hex.Analysis.Typecheck
  ( Type
  , TypeF(..)
  , TcState(..)
  , Attrs(..)
  , defaultAttrs
  , newTcState
  , runTypecheck
  , execTypecheck
  , tc ) where

import Control.Monad.Except (runExceptT)
import Control.Monad.State hiding (void)
import Control.Unification
import Control.Unification.IntVar
import Control.Unification.Types

import Data.Functor.Fixedpoint (Fix(..))
import Data.Functor.Identity
import Data.List (intercalate)
import Data.Maybe (fromMaybe)
import Debug.Trace (trace)

import qualified Hex.Analysis.Symtab as S
import Hex.Pretty
import Hex.Term


-- deriving @Generic1@ allows you to automatically derive @Unifiable@
-- because you can recurse across the structure of @TypeF@ in a pretty
-- obvious and generic way (~somehow~)
-- | A functor that represents types.
data TypeF a = BaseType String
             | Fn [a] a
  deriving (Foldable, Functor, Show, Traversable)

instance Unifiable TypeF where
  zipMatch a b =
    case a of
      BaseType a' -> do
        BaseType b' <- return b
        guard (a' == b')
        return (BaseType a')
      Fn a0 a1 -> do
        Fn b0 b1 <- return b
        return $ Fn (zipWith (\a b -> Right (a, b)) a0 b0) (Right (a1, b1))

instance (Pretty a) => Pretty (TypeF a) where
  pretty (BaseType s) = s
  pretty (Fn a b)     = printf "λ %s. %s" (pretty' a) (pretty b)

-- | The abstraction that we use to handle types.
type Type = UTerm TypeF IntVar

-- | An cleaner way of constructing function types.
(~>) :: [Type] -> Type -> Type
a ~> b = UTerm (Fn a b)

instance Pretty IntVar where
  pretty (IntVar n) = show n

instance (Pretty a) => Pretty (UTerm TypeF a) where
  pretty (UVar v)  = '\'' : pretty v
  pretty (UTerm t) = pretty t


-- chicken noodle soup, chicken noodle soup, chicken noodle soup with a monad on the side
newtype Typecheck a =
  Typecheck { unTypecheck :: StateT TcState (S.SymbolT (IntBindingT TypeF Identity) Attrs) a }
  deriving (Functor, Applicative, Monad, MonadState TcState)

data TcState = TcState { returns :: [Type] }

newTcState :: TcState
newTcState = TcState { returns = [] }


data Attrs = Attrs { att_type  :: Type }

instance Pretty Attrs where
  pretty Attrs{..} = printf "%s" type'
    where type' = pretty att_type


defaultAttrs :: Attrs
defaultAttrs = Attrs { att_type  = UTerm (BaseType "") }


-- ~I apologize for this~
runTypecheck :: Typecheck a -> ((a, TcState), S.SymtabState Attrs)
runTypecheck =
  runIdentity
  . evalIntBindingT
  . S.runSymbolT
  . (flip runStateT) newTcState
  . unTypecheck

execTypecheck :: Typecheck a -> a
execTypecheck = fst . fst . runTypecheck


-- these are our base types; they're predefined for convenience.
double = UTerm (BaseType "double")
nil    = UTerm    (BaseType "nil")
void   = UTerm   (BaseType "void")


tc :: Term -> Typecheck (ATerm Attrs)
tc (Fix t) =
  case t of
    Void -> return (giveType void Void)
    
    Dbl{..} -> do
      let t' = Dbl tdbl_val
      return (giveType double t')

    Def{..} -> do
      val <- tc tdef_val
      insert tdef_name (ann val)
      let t' = giveType nil (t { tdef_val = val })
      return t'

    Ret{..} -> do
      val <- tc tret_val
      addReturn (typeOf val)
      let t' = t { tret_val = val }
      return (giveType nil t')

    Var{..} -> do
      a <- findJust tvar_name
      let ty = att_type a
          t' = Var tvar_name
      return (giveType ty t')

    Apply{..} -> do
      -- the "officially recorded" type of the function
      ty0 <- case builtinType tap_name of
               Just ty -> return ty
               Nothing -> do
                 a <- findJust tap_name
                 return (att_type a)
      -- the type of the function as used in this application
      args   <- mapM tc tap_args
      v      <- uvar
      let argTys = map typeOf args
          ty1    = UTerm (Fn argTys v)
      -- try to unify our two types and get the return value
      result <- unify' ty0 ty1
      case result of
        Left _ ->
          let p0 = pretty ty0
              p1 = pretty ty1
          in error ("Could not unify types: " ++ p0 ++ " ~ " ++ p1)
        -- TODO: process results
        Right a -> trace (pretty a) (return undefined)

  
    FnDef{..} -> withFn $ do
      bindArgs tfn_args
      mapM tc tfn_body
      ret <- unifyReturns
      -- resolve variables and construct type
      return undefined

    _ -> error "can't typecheck this"



-- | Determines the type of a builtin function, returning @Nothing@
-- if the supplied function isn't a builtin function.
builtinType :: String -> Maybe Type
builtinType "+" = Just (UTerm $ Fn
                        [ UTerm $ BaseType "double"
                        , UTerm $ BaseType "double" ]
                        (UTerm $ BaseType "double"))
builtinType  _  = Nothing

-- | Extracts the type from an annotated term.
typeOf :: ATerm Attrs -> Type
typeOf = att_type . ann

-- | Annotate a term with a type.
giveType :: Type -> TermF (ATerm Attrs) -> ATerm Attrs
giveType ty t = annotate attrs t
  where attrs = defaultAttrs { att_type = ty }


-- | Performs a typechecking action within the context of a function.
withFn :: Typecheck a -> Typecheck a
withFn f = do
  result <- f
  cleanup
  return result
    where cleanup = modify (\s -> s { returns = [] })

-- | Add a return type to the current list.
addReturn :: Type -> Typecheck ()
addReturn ty = modify (\s -> s { returns = ty : (returns s) })

-- | Bind a list of argument names to new type variables.
bindArgs :: [String] -> Typecheck [(String, Type)]
bindArgs args =
  forM args $ \name -> do
  ty <- uvar
  insert name (Attrs ty)
  return (name, ty)

-- | Unify the active return types into a single type.
unifyReturns :: Typecheck Type
unifyReturns = do
  types <- gets returns
  ty <- foldM unify types
  applyBindings ty
  


-- below are functions that are "auto-lifted" into the Typecheck monad.
-- this is done to avoid the noise of "Typecheck . lift . lift . lift $ ...." bits
liftSym = Typecheck . lift

-- | Looks up a symbol by name.
find :: String -> Typecheck (Maybe Attrs)
find s = liftSym (S.find s)

-- | Like @find@, but throws an error if the variable doesn't exist.
findJust :: String -> Typecheck Attrs
findJust s = liftM (fromMaybe err) (find s)
  where err = error ("undefined variable " ++ s)


-- | Inserts a symbol's information by name.
insert :: String -> Attrs -> Typecheck ()
insert s a = liftSym (S.insert s a)

-- | Adds another scope level to the symbol table.
pushScope :: Typecheck ()
pushScope = liftSym S.pushScope

-- | Removes the current scope level from the symbol table.
popScope :: Typecheck ()
popScope = liftSym S.popScope


-----
liftUnify = Typecheck . lift . S.Symtab . lift

-- | Create a free unification variable.
uvar :: Typecheck Type
uvar = liftUnify (fmap UVar freeVar)

-- | Lifted unification.
unify' :: Type -> Type -> Typecheck (Either (UFailure TypeF IntVar) Type)
unify' a b = liftUnify $ runExceptT (unify a b >>= applyBindings)
