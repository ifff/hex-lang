{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Hex.Codegen.Types where

import Control.Monad.State
import Control.Applicative

import qualified Data.Map as Map

import LLVM.AST
import qualified LLVM.AST as AST

import Hex.Util

---------------------------------------------------------------
-- CODEGEN
---------------------------------------------------------------
type Names = Map.Map String Int

type SymbolTable = [(String, Operand)]

data CodegenState = CodegenState
  { currentBlock :: Name -- ^ The name of the current block being modified.
  , blocks       :: Map.Map Name BlockState -- ^ A list of all blocks, by name.
  , symtab       :: SymbolTable -- ^ A list of local variables.
  , nBlocks      :: Int -- ^ The number of blocks.
  , count        :: Word -- ^ A counter (for naming things).
  , names        :: Names -- ^ A list of all the names that have been defined.
  } deriving Show

emptyCodegen :: CodegenState
emptyCodegen = CodegenState
  { currentBlock = Name (toShort entryBlockName)
  , blocks       = Map.empty
  , symtab       = []
  , nBlocks      = 1
  , count        = 0
  , names        = Map.empty }

newtype Codegen a = Codegen { runCodegen :: State CodegenState a }
  deriving (Functor, Applicative, Monad, MonadState CodegenState)

execCodegen :: Codegen a -> CodegenState
execCodegen m = execState (runCodegen m) emptyCodegen



---------------------------------------------------------------
-- BLOCK STATE
---------------------------------------------------------------
data BlockState = BlockState
  { idx   :: Int
  , stack :: [Named Instruction]
  , term  :: Maybe (Named Terminator)
  } deriving Show

entryBlockName :: String
entryBlockName = "entry"


---------------------------------------------------------------
-- LLVM MONAD
---------------------------------------------------------------
newtype LLVM a = LLVM (State AST.Module a)
  deriving (Functor, Applicative, Monad, MonadState AST.Module)

runLLVM :: AST.Module -> LLVM a -> AST.Module
runLLVM mod (LLVM m) = execState m mod

emptyModule :: String -> AST.Module
emptyModule lbl = defaultModule { moduleName = toShort lbl }


---------------------------------------------------------------
-- PRIMITIVE TYPES
---------------------------------------------------------------
double :: Type
double = FloatingPointType DoubleFP
