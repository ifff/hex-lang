{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Hex.Codegen (codegen) where

import Data.Functor.Fixedpoint (Fix(..))
import Data.Word (Word)
import Data.String
import qualified Data.Map as Map

import LLVM.AST
import LLVM.AST.Global
import qualified LLVM.AST as AST

import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.Linkage as L
import qualified LLVM.AST.CallingConvention as CC
import qualified LLVM.AST.FloatingPointPredicate as FP

import Hex.Analysis.Typecheck
import Hex.Codegen.Instrs
import Hex.Codegen.Types
import Hex.Codegen.Util
import Hex.JIT
import Hex.Term

import Debug.Trace (trace)

-- | Compiles and runs an LLVM module holding the provided @Term@s.
codegen :: AST.Module -> [ATerm Attrs] -> IO AST.Module
codegen mod fns = runJIT oldast
  where
    modn    = mapM codegenTop fns
    oldast  = runLLVM mod modn

-- | Handles code generation for top-level constructs, e.g. function declarations.
codegenTop :: ATerm Attrs -> LLVM ()
codegenTop Annotate{..} =
  case layer of
    Def{..} -> do
      let val = constGen tdef_val
      define' double tdef_name val
    _ -> error "unimplemented codegen rule!"
  
-- codegenTop (S.Function name args body) = define double name fnargs bs
--   where fnargs = toSig args
--         bs = createBlocks $ execCodegen $ do
--           entry <- addBlock entryBlockName
--           setBlock entry
--           forM args $ \a -> do
--             var <- alloca double
--             store var (local (AST.Name a))
--             assign a var
--           cgen body >>= ret

-- codegenTop (S.Extern name args) = external double name fnargs
--   where fnargs = toSig args

-- the default case defines `main` and puts the expr inside
-- codegenTop expr = define double "main" [] bs
--   where bs = createBlocks $ execCodegen $ do
--           entry <- addBlock entryBlockName
--           setBlock entry
--           cgen expr >>= ret

-- | Recursively generates LLVM code for @Term@s.
cgen :: ATerm Attrs -> Codegen AST.Operand
cgen Annotate{..} =
  case layer of
    Dbl{..} -> return $ con (dbl tdbl_val)
    _ -> error "unimplemented codegen rule!"
-- cgen (S.Var x) = getvar x >>= load
-- cgen (S.Call f args) = do
--   largs <- mapM cgen args
--   call (externf (AST.Name f) largs)

-- codegen :: AST.Module -> [Term] -> IO AST.Module
-- codegen mod fns = withCtx $ \ctx ->
--   liftErr $ withModuleFromAST ctx newast $
--   \m -> do
--     llstr <- moduleLLVMAssembly m
--     putStrLn llstr
--     return newast
--   where modn = mapM codegenTop fns
--         runLLVM mod modn
 

-- | Generates code for a constant value.
constGen :: ATerm Attrs -> C.Constant
constGen Annotate{..} =
  case layer of
    Dbl{..} -> dbl tdbl_val
    _ -> error "unimplemented constgen rule!"
