{ mkDerivation, base, bytestring, containers, HUnit, lens, llvm-hs
, llvm-hs-pure, mtl, parsec, stdenv, transformers, unification-fd
, utf8-string
}:
mkDerivation {
  pname = "hex-lang";
  version = "1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base bytestring containers lens llvm-hs llvm-hs-pure mtl parsec
    transformers unification-fd utf8-string
  ];
  testHaskellDepends = [ base HUnit parsec ];
  homepage = "https://gitlab.com/ifff/hex#hex";
  description = "An experimental programming language";
  license = stdenv.lib.licenses.gpl3;
}
